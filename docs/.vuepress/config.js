module.exports = {
    /**
     * 基本配置
     */
    // 部署站点的基础路径，如果你想让你的网站部署到一个子路径下，你将需要设置它。
    base: '/',
    // 网站的标题，它将会被用作所有页面标题的前缀，同时，默认主题下，它将显示在导航栏（navbar）上。
    title: 'EasySpring 中文文档',
    // 网站的描述，它将会以 <meta> 标签渲染到当前页面的 HTML 中。
    description: '简洁编码, 从现在开始.',
    // 指定 vuepress build 的输出目录。
    // dest: '.vuepress/dist',
    locales: {
        // 键名是该语言所属的子路径
        // 作为特例，默认语言可以使用 '/' 作为其路径。
        '/': {
            lang: 'zh-CN',
            title: 'EasySpring',
            description: '本站是EasySpring(李显)的项目框架网站. 主要用于 Java 后台的快速开发, 加快开发的效率, 节省开发时间.'
        }
    },
    head: [
        // icon 图标的配置
        ['link', {rel: 'icon', href: '/favicon.ico'}],
        // 设置手机视图自适应
        ['meta', {name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no'}],
        // 搜索优化
        // 关键字设置
        ['meta', {name: 'keyword', content: 'EasySpring,SpringBoot,SpringCloud'}],
        // 简介
        ['meta', {name: 'description', content: '本站是EasySpring(李显)的项目框架网站. 主要用于 Java 后台的快速开发, 加快开发的效率, 节省开发时间.'}],
        // SEO 优化
        ['meta', {property: 'og:type', content: 'website'}],
        ['meta', {property: 'og:title', content: 'EasySpring'}],
        ['meta', {property: 'og:url', content: 'https://www.easyspring.io'}],
        ['meta', {property: 'og:site_name', content: 'EasySpring'}],
        ['meta', {property: 'og:description', content: '本站是EasySpring(李显)的项目框架网站. 主要用于 Java 后台的快速开发, 加快开发的效率, 节省开发时间.'}],
        ['meta', {property: 'og:locale', content: 'zh-CN'}],
        // 百度统计的相关配置
        [
            "script",
            {},
            `
                var _hmt = _hmt || [];
                (function() {
                    var hm = document.createElement("script");
                    hm.src = "https://hm.baidu.com/hm.js?294fb48ee355cd6104c38d5e047fad56";
                    var s = document.getElementsByTagName("script")[0];
                    s.parentNode.insertBefore(hm, s);
                })();
            `
        ]
    ],

    /**
     * 主题样式的配置
     */
    themeConfig: {
        nav: [
            {text: '指南', link: '/guide/'},
            {text: '整体架构', link: '/framework/'},
            {
                text: 'Framework',
                items: [
                    {text: 'Base', link: '/framework/base/'},
                    {text: 'Common', link: '/framework/common/'},
                ]
            },
            {
                text: '了解更多',
                ariaLabel: '了解更多',
                items: [
                    {
                        text: 'API',
                        items: [
                            {
                                text: 'CLI',
                                link: '/zh/api/cli.html'
                            },
                            {
                                text: 'Node',
                                link: '/zh/api/node.html'
                            }
                        ]
                    },
                    {
                        text: '开发指南',
                        items: [
                            {
                                text: '本地开发',
                                link: '/zh/miscellaneous/local-development.html'
                            },
                            {
                                text: '设计理念',
                                link: '/zh/miscellaneous/design-concepts.html'
                            },
                            {
                                text: 'FAQ',
                                link: '/zh/faq/'
                            },
                            {
                                text: '术语',
                                link: '/zh/miscellaneous/glossary.html'
                            }
                        ]
                    },
                    {
                        text: '其他',
                        items: [
                            {
                                text: '从 0.x 迁移',
                                link: '/zh/miscellaneous/migration-guide.html'
                            },
                            {
                                text: 'Changelog',
                                link: 'https://github.com/vuejs/vuepress/blob/master/CHANGELOG.md'
                            }
                        ]
                    }
                ]
            },
            {text: 'API', link: 'https://apidoc.gitee.com/easyspring/spring-book'},
            {text: 'Gitee', link: 'https://gitee.com/easyspring/spring-book'},
            {text: '博客', link: 'https://blog.easyspring.io'},
        ],
        logo: '/head.png',
        // sidebar: [
        //     '/',
        //     '/framework/common/',
        //     ['/framework/base/', 'Explicit link text']
        // ],
        sidebar: 'auto',
        displayAllHeaders: true,
        //搜索
        search: true,
        searchMaxSuggestions: 10,
        sidebarDepth: 3,
        // 最后更新时间
        lastUpdated: '上次更新时间', // string | boolean
    },

    /**
     * markdown 配置
     */
    markdown: {
        // 是否显示行号
        lineNumbers: true
    }
};
